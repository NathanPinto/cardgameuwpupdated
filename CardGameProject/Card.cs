﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public enum CardSuit
	{
		DIAMONDS = 1,
		CLUBS,
		HEARTS,
		SPADES
	}

	/// <summary>
	/// Represents a card game playing card
	/// </summary>
	public class Card
	{
		/// <summary>
		/// The value of the card, 1 -> 13
		/// </summary>
		private byte _value;

		/// <summary>
		/// The suit of the card
		/// </summary>
		private CardSuit _suit;
	}
}
